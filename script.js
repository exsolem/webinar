window.onload = function (){
    let days = document.querySelectorAll( '.days' ),
        hours = document.querySelectorAll( '.hours' ),
        minutes = document.querySelectorAll( '.minutes' ),
        seconds = document.querySelectorAll( '.seconds' );
    const webinar_date = new Date(2019, 6, 20, 20, 60, 60);


    setInterval( function () {
        let date = new Date();
        days. forEach( item => item.innerHTML = date.getDate() <= 9 ? `0${date.getDate()}`: date.getDate() );
        hours.forEach( item => item.innerHTML = date.getHours() <= 9 ? `0${date.getHours()}` : date.getHours() );
        minutes.forEach( item => item.innerHTML = date.getMinutes() <= 9 ? `0${date.getMinutes()}` : date.getMinutes() );
        seconds.forEach( item => item.innerHTML = date.getSeconds() <= 9 ? `0${date.getSeconds()}` : date.getSeconds() );
    },500 )
    
console.log(webinar_date.getSeconds());

    let registrationCloseButton = document.querySelector( '#hidden_window > #registration > button' ),
        hidden_window = document.querySelector( '#hidden_window' ),
        registration = document.querySelector( '#registration' ),
        ready_btn = document.querySelectorAll( '.ready_btn' ),
        reg_btn = document.querySelector( '#reg_btn' ),
        reg_form = document.querySelector( '#reg_form' );
    console.log(ready_btn);
    ready_btn.forEach( ( item ) => item.addEventListener( 'click', showWindow ) )
    

    function showWindow(){
        registration.style.display = 'flex';
        hidden_window.style.display = 'flex';
        setTimeout(function () {
            hidden_window.style.opacity = '1';
            registration.style.opacity = '1';
        }, 150 )
    }

    registrationCloseButton.addEventListener( 'click' , hideWindow );
   // reg_form.elements.password.addEventListener( 'blur' , passwordChange );
    //reg_form.elements.login.addEventListener( 'blur' , passwordChange );
    //reg_btn.addEventListener( 'click' , reg_validate );

    function passwordChange (){
        if( reg_form.elements.password.value ){
            reg_btn.disabled = false;
            reg_form.elements.password.style.border = '';
            reg_btn.style.backgroundColor = '';
        }
        else {
            reg_btn.disabled = true;
            reg_btn.style.backgroundColor = '#363636';
            reg_form.elements.password.placeholder = 'ВВЕДИТЕ ПАРОЛЬ';
            reg_form.elements.password.style.border = '2px solid red';
        }
    }
    function reg_validate(){
        if( reg_form.elements.password.value ){
            reg_btn.disabled = false;
            reg_form.elements.password.style.border = '';
            hideWindow();
        }
        else {
            reg_btn.disabled = true;
            reg_form.elements.password.placeholder = 'ВВЕДИТЕ ПАРОЛЬ';
            reg_form.elements.password.style.border = '2px solid red';
        }
        
    }

    function hideWindow(){
        //hidden_window.style.left = '-1000px';
        hidden_window.style.opacity = '0';
        registration.style.opacity = '0';

        setTimeout( () => {
            hidden_window.style.display = 'none';
            hidden_window.style.display = 'none';
        }, 500 );
        
    }
   
    let questions = document.querySelector( '#questions' ),
        questionsClosebtn = document.querySelector( '#qustionsCloseButton' ),
        qustionFormInput = document.querySelector( '#qusetions_form_input' ),
        questionBtn = document.querySelectorAll( '.question_button' );

       // qustionFormInput.elements.send_btn.addEventListener('click', question_send);
        questionsClosebtn.addEventListener( 'click', question_send );
        questionBtn.forEach( ( item ) => {
            console.log(item);
            item.addEventListener('click', showQuestionWindow);
        });

        function showQuestionWindow () {
            hidden_window.style.display = 'flex';
            questions.style.display = "flex";
            setTimeout(() => {
                questions.style.opacity = "1";
                hidden_window.style.opacity = '1';
            },100); 
        }

        function question_send (){
            questions.style.opacity = "0";
            hidden_window.style.opacity = '0';

            setTimeout(() => {
                hidden_window.style.display = 'none';
                questions.style.display = "none";
            },600); 
        }

        let education_close_btn = document.querySelector( '#education_close_btn' ),
            education_programm_window = document.querySelector( '#education_programm' ),
            education_programm_send = document.querySelector( '#education_programm_send' );
        
        education_close_btn.addEventListener( 'click', hide_education_window );
        education_programm_send.addEventListener( 'click', show_education_window );


        function show_education_window () {

            hidden_window.style.display = 'flex';
            education_programm_window.style.display = "flex";
            setTimeout(() => {
                education_programm_window.style.opacity = "1";
                hidden_window.style.opacity = '1';
            },100 ); 
        }

        function hide_education_window () {
            education_programm_window.style.opacity = "0";
            hidden_window.style.opacity = '0';

            setTimeout( () => {
                hidden_window.style.display = 'none';
                education_programm_window.style.display = "none";
            },600 ); 
        }

        let registration_btn =  document.querySelector('#registration_btn'),
            login_window = document.querySelector('#login_window'),
            login_window_close = document.querySelector('#login_window_close');

        registration_btn.addEventListener('click', show_reg_windiw);
        login_window_close.addEventListener('click', hide_reg_window);

        function show_reg_window (){
            hidden_window.style.display = 'flex';
            login_window.style.display = "flex";
            setTimeout(() => {
                login_window.style.opacity = "1";
                hidden_window.style.opacity = '1';
            },100 ); 
        }

        function hide_reg_window () {
            login_window.style.opacity = "0";
            hidden_window.style.opacity = '0';

            setTimeout( () => {
                hidden_window.style.display = 'none';
                login_window.style.display = "none";
            },600 ); 
        }
        hideWindow();   
        hide_education_window();
        question_send();
        hide_reg_window ();
}


